package com.ubykuo.podium

import android.app.Application

class PodiumApp : Application() {

    companion object {

        private var internalInstance: PodiumApp? = null

        val instance: PodiumApp get() = internalInstance ?: throw IllegalStateException("Podium App instance is not initialized")
    }

    override fun onCreate() {
        super.onCreate()
        internalInstance = this
        Storage.init(this)
    }
}