package com.ubykuo.podium

import android.os.Parcelable
import com.auth0.android.jwt.JWT
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

sealed class AsyncResource<out T>
data class Success<out T>(val data: T) : AsyncResource<T>()
class Loading<out T> : AsyncResource<T>()
data class Error<out T>(val error: Throwable, val message: String) : AsyncResource<T>()

data class PodiumToken(@field:Json(name = "auth_token") val token: String) {
    val decoded: User? get() = JWT(token).getClaim("user").asString()?.let { User(it) }
}

data class User(val email: String)
@Parcelize data class Podium(val name: String, val image: String, val tasks: List<Task>) : Parcelable
@Parcelize data class Task(val priority: Int, val description: String): Parcelable
data class PodiumListResponse(val projects: List<Podium>)
