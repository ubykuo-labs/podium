package com.ubykuo.podium.detail

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.ubykuo.podium.Podium
import com.ubykuo.podium.R
import kotlinx.android.synthetic.main.activity_podium_detail.*

class PodiumDetailActivity : AppCompatActivity() {

    companion object {
        const val PODIUM_EXTRA = "podium-detail-activity-podium-extra"
        const val PODIUM_TRANSITION_NAME_EXTRA = "podium-detail-activity-podium-transition"
    }

    private val podium: Podium by lazy { intent.getParcelableExtra(PODIUM_EXTRA) as Podium }
    private val podiumImageTransition: String by lazy { intent.getStringExtra(PODIUM_TRANSITION_NAME_EXTRA) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_podium_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(false)
            it.setDisplayHomeAsUpEnabled(true)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = Color.WHITE
        }
        projectImage.transitionName = podiumImageTransition
        Glide.with(this).load(podium.image).into(projectImage)
        projectName.text = podium.name
        okButton.setOnClickListener { onBackPressed() }
        podium.tasks.filter { it.priority <= 3 }
                .forEach {
                    when (it.priority) {
                        1 -> topPriorityText.text = it.description
                        2 -> middlePriorityText.text = it.description
                        3 -> lastPriorityText.text = it.description
                    }
                }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        supportFinishAfterTransition()
    }
}
