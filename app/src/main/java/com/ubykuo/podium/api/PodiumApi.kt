package com.ubykuo.podium.api

import com.ubykuo.podium.PodiumListResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header

interface PodiumApi {

    @GET("projects")
    fun projects(@Header("Authorization") token: String): Observable<PodiumListResponse>
}