package com.ubykuo.podium.api

import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

object ApiManager {

    val authApi: AuthApi by lazy { retrofit.create(AuthApi::class.java) }
    val podiumApi: PodiumApi by lazy { retrofit.create(PodiumApi::class.java) }

    private val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://dev.fukumochi.org:4512/api/")
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build())
            .build()
}