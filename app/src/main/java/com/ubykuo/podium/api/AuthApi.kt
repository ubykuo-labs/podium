package com.ubykuo.podium.api

import com.ubykuo.podium.PodiumToken
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthApi {

    @POST("auth/login")
    @FormUrlEncoded
    fun login(@Field("email") email: String,
              @Field("password") password: String): Observable<PodiumToken>
}