package com.ubykuo.podium

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    protected val rubbishBin = CompositeDisposable()

    override fun onCleared() {
        rubbishBin.clear()
    }
}