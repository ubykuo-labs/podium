package com.ubykuo.podium.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ubykuo.podium.*
import com.ubykuo.podium.main.MainActivity
import com.ubykuo.podium.widgets.LoginCardView
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginCardView.Listener {

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginCard.listener = this
        viewModel = ViewModelProviders.of(this)[LoginViewModel::class.java]
        viewModel.validationEvents.observe(this, Observer { validation ->
            validation?.let { loginCard.showValidation(it) }
        })
        viewModel.loginEvents.observe(this, Observer { onLoginResult(it) })
    }

    override fun onLoginClick() {
        viewModel.login(loginCard.email, loginCard.password)
    }

    private fun onLoginResult(resource: AsyncResource<User>?) {
        when (resource) {
            is Loading -> loginCard.loading()
            is Error -> with(loginCard) {
                resetState()
                snack(resource.message)
            }
            is Success -> {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }
    }
}
