package com.ubykuo.podium.login

import com.ubykuo.podium.*
import com.ubykuo.podium.api.ApiManager
import io.reactivex.Observable
import retrofit2.HttpException

class LoginUseCase(private val email: String, private val password: String) : UseCase<User> {

    private val invalidCredentials: String by lazy { PodiumApp.instance.getString(R.string.invalid_credentials) }
    private val serverError: String by lazy { PodiumApp.instance.getString(R.string.server_error) }
    private val unexpectedError: String by lazy { PodiumApp.instance.getString(R.string.unexpected_error) }

    override fun execute(): Observable<AsyncResource<User>> =
            Observable.concat<AsyncResource<User>>(
                    Observable.just(Loading()),
                    ApiManager.authApi.login(email, password)
                            .doOnNext { Storage.instance.token = it }
                            .flatMap {
                                if (it.decoded != null) Observable.just(Success(it.decoded!!))
                                else Observable.error(Throwable("Auth token couldn't be decoded"))
                            })
                    .onErrorReturn {
                        if (it is HttpException) when {
                            it.code() == 401 -> Error(it, invalidCredentials)
                            it.code() >= 500 -> Error(it, serverError)
                            else -> Error(it, it.message ?: unexpectedError)
                        } else Error(it, it.message ?: unexpectedError)
                    }
}