package com.ubykuo.podium.login

import android.arch.lifecycle.MutableLiveData
import com.ubykuo.podium.*
import com.ubykuo.podium.login.LoginViewModel.LoginValidationResult.*
import io.reactivex.schedulers.Schedulers

class LoginViewModel : BaseViewModel() {

    enum class LoginValidationResult { SUCCESS, EMAIL_EMPTY, EMAIL_INVALID, PASSWORD_EMPTY }

    val validationEvents = MutableLiveData<LoginValidationResult>()
    val loginEvents = MutableLiveData<AsyncResource<User>>()

    fun login(email: String, password: String) {
        validationEvents.value = when {
            email.isEmpty() -> EMAIL_EMPTY
            !email.isValidEmail -> EMAIL_INVALID
            password.isEmpty() -> PASSWORD_EMPTY
            else -> SUCCESS
        }
        if (validationEvents.value == SUCCESS) LoginUseCase(email, password).execute()
                .subscribeOn(Schedulers.io())
                .subscribe(loginEvents::postValue)
                .addTo(rubbishBin)
    }
}