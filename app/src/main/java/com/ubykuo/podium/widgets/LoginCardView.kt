package com.ubykuo.podium.widgets

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import com.ubykuo.podium.R
import com.ubykuo.podium.login.LoginViewModel
import com.ubykuo.podium.login.LoginViewModel.LoginValidationResult.*
import kotlinx.android.synthetic.main.card_login.view.*

class LoginCardView(context: Context, attrs: AttributeSet? = null) : CardView(context, attrs) {

    interface Listener {
        fun onLoginClick()
    }

    val email: String get() = emailInput.text.toString()
    val password: String get() = passwordInput.text.toString()
    var listener: Listener? = null

    private val emptyMessage: String by lazy { context.getString(R.string.empty_field) }
    private val invalidEmailMessage: String by lazy { context.getString(R.string.invalid_email) }

    init {
        View.inflate(context, R.layout.card_login, this)
        loginButton.setOnClickListener { listener?.onLoginClick() }
    }

    fun showValidation(validationResult: LoginViewModel.LoginValidationResult) {
        when (validationResult) {
            EMAIL_EMPTY -> setErrors(emptyMessage, null)
            EMAIL_INVALID -> setErrors(invalidEmailMessage, null)
            PASSWORD_EMPTY -> setErrors(null, emptyMessage)
            else -> setErrors(null, null)
        }
    }

    fun loading() {
        emailInput.isEnabled = false
        passwordInput.isEnabled = false
        loginButton.visibility = View.INVISIBLE
        progressBar.visibility = View.VISIBLE
    }

    fun resetState() {
        setErrors(null, null)
        emailInput.isEnabled = true
        passwordInput.isEnabled = true
        progressBar.visibility = View.INVISIBLE
        loginButton.visibility = View.VISIBLE
    }

    private fun setErrors(emailError: String?, passwordError: String?) {
        emailInputLayout.error = emailError
        passwordInputLayout.error = passwordError
    }
}