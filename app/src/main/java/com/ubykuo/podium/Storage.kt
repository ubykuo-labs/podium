package com.ubykuo.podium

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi

class Storage private constructor(private val prefs: SharedPreferences) {

    companion object {

        val instance: Storage get() = internalInstance ?: throw IllegalStateException("Storage is not initialized")

        private var internalInstance: Storage? = null

        fun init(context: Context) {
            if (internalInstance == null) {
                internalInstance = Storage(context.getSharedPreferences("podium_prefs", Context.MODE_PRIVATE))
            }
        }
    }

    private val tokenKey = "token"

    var token: PodiumToken?
        get() = prefs.getString(tokenKey, "").let { if (it.isNotEmpty()) PodiumToken(it) else null }
        set(value) { prefs.edit().putString(tokenKey, value?.token ?: "").apply() }
}