package com.ubykuo.podium

import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

val String.isValidEmail: Boolean get() = Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun Disposable.addTo(disposables: CompositeDisposable) {
    disposables.add(this)
}

fun View.snack(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(this, message, duration).show()
}

fun ViewGroup.inflate(@LayoutRes layout: Int): View =
        LayoutInflater.from(context).inflate(layout, this, false)