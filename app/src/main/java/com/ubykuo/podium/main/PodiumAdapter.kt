package com.ubykuo.podium.main

import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.ubykuo.podium.Podium
import com.ubykuo.podium.R
import com.ubykuo.podium.inflate
import kotlinx.android.synthetic.main.card_podium.view.*

class PodiumAdapter(private var podiums: List<Podium>, private var listener: Listener) : RecyclerView.Adapter<PodiumAdapter.ViewHolder>() {

    interface Listener {
        fun onPodiumClicked(podium: Podium, podiumImageView: ImageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(parent.inflate(R.layout.card_podium))

    override fun getItemCount(): Int = podiums.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindTo(podiums[position])
    }

    fun replacePodiums(newPodiums: List<Podium>) {
        podiums = newPodiums
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindTo(podium: Podium) {
            Glide.with(itemView.context)
                    .load(podium.image)
                    .into(itemView.cardProjectImage)
            ViewCompat.setTransitionName(itemView.cardProjectImage, podium.name)
            itemView.cardProjectText.text = podium.name
            podium.tasks.filter { it.priority <= 3 }
                    .forEach {
                        when (it.priority) {
                            1 -> itemView.cardTopPriorityText.text = it.description
                            2 -> itemView.cardMiddlePriorityText.text = it.description
                            3 -> itemView.cardLastPriorityText.text = it.description
                        }
                    }
            itemView.setOnClickListener { listener.onPodiumClicked(podium, itemView.cardProjectImage) }
        }
    }
}