package com.ubykuo.podium.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import android.widget.ImageView
import com.ubykuo.podium.*
import com.ubykuo.podium.detail.PodiumDetailActivity
import com.ubykuo.podium.login.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), PodiumAdapter.Listener {

    private lateinit var viewModel: MainViewModel
    private lateinit var podiumAdapter: PodiumAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(mainToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        viewModel = ViewModelProviders.of(this)[MainViewModel::class.java]
        viewModel.loggedInEvents.observe(this, Observer { onLoggedInCheckResult(it) })
        viewModel.podiumListEvents.observe(this, Observer { onPodiumsResult(it) })
        lifecycle.addObserver(viewModel)
        podiumAdapter = PodiumAdapter(emptyList(), this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = podiumAdapter
        addButton.setOnClickListener { addButton.snack("Hoy no vamos a implementar esto :)") }
    }

    private fun onPodiumsResult(resource: AsyncResource<List<Podium>>?) {
        when (resource) {
            is Loading -> setVisibility(progressVisible = true)
            is Error -> setVisibility(errorVisible = true)
            is Success -> {
                podiumAdapter.replacePodiums(resource.data)
                setVisibility(recyclerVisible = true)
            }
        }
    }

    private fun onLoggedInCheckResult(loggedIn: Boolean?) {
        if (loggedIn == null || !loggedIn) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun setVisibility(recyclerVisible: Boolean = false,
                              errorVisible: Boolean = false,
                              progressVisible: Boolean = false) {
        fun visibilityFrom(visible: Boolean): Int = if (visible) View.VISIBLE else View.INVISIBLE
        recyclerView.visibility = visibilityFrom(recyclerVisible)
        errorText.visibility = visibilityFrom(errorVisible)
        progressBar.visibility = visibilityFrom(progressVisible)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        R.id.action_logout -> {
            viewModel.logout()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onPodiumClicked(podium: Podium, podiumImageView: ImageView) {
        val transitionName = ViewCompat.getTransitionName(podiumImageView)
        val intent = Intent(this, PodiumDetailActivity::class.java)
                .putExtra(PodiumDetailActivity.PODIUM_EXTRA, podium)
                .putExtra(PodiumDetailActivity.PODIUM_TRANSITION_NAME_EXTRA, transitionName)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, podiumImageView, transitionName)
        startActivity(intent, options.toBundle())
    }
}
