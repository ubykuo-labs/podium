package com.ubykuo.podium.main

import android.arch.lifecycle.*
import com.auth0.android.jwt.JWT
import com.ubykuo.podium.*
import io.reactivex.schedulers.Schedulers
import java.util.*

class MainViewModel : BaseViewModel(), LifecycleObserver {

    val loggedInEvents = MutableLiveData<Boolean>()
    val podiumListEvents = MutableLiveData<AsyncResource<List<Podium>>>()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onViewCreated() {
        val token = Storage.instance.token
        loggedInEvents.value = if (token != null && Date().before(JWT(token.token).expiresAt)) {
            fetchPodiums()
            true
        } else false
    }

    private fun fetchPodiums() {
        GetPodiumListUseCase().execute()
                .subscribeOn(Schedulers.io())
                .subscribe(podiumListEvents::postValue)
                .addTo(rubbishBin)
    }

    fun logout() {
        Storage.instance.token = null
    }
}