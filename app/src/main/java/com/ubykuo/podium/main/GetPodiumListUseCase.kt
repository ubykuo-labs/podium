package com.ubykuo.podium.main

import com.ubykuo.podium.*
import com.ubykuo.podium.api.ApiManager
import io.reactivex.Observable

class GetPodiumListUseCase : UseCase<List<Podium>> {

    private val unexpectedError: String by lazy { PodiumApp.instance.getString(R.string.unexpected_error) }

    override fun execute(): Observable<AsyncResource<List<Podium>>> =
            Observable.concat(
                    Observable.just(Loading()),
                    ApiManager.podiumApi.projects(Storage.instance.token!!.token)
                            .map { Success(it.projects) as AsyncResource<List<Podium>> })
                    .onErrorReturn { Error(it, it.message ?: unexpectedError) }
}