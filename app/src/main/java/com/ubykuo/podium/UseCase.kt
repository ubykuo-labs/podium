package com.ubykuo.podium

import io.reactivex.Observable

interface UseCase<T> {
    fun execute(): Observable<AsyncResource<T>>
}